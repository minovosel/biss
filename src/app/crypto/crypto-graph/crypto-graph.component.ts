import {Component, OnInit, ViewChild} from '@angular/core';
import {Chart} from 'chart.js';
import {CryptoModel} from '../models/crypto.model';
import {Subscription} from 'rxjs';
import {CryptoService} from '../services/crypto.service';
import {BaseChartDirective} from 'ng2-charts';

@Component({
  selector: 'app-crypto-graph',
  templateUrl: './crypto-graph.component.html',
  styleUrls: ['./crypto-graph.component.scss']
})
export class CryptoGraphComponent implements OnInit {
  chartLabels: Array<number>;
  chartOptions = {
    responsive: true
  };
  chartData: Array<{ data: Array<number>; label: string }>;
  prevInterval: number;

  getCryptoValuesSubscription: Subscription;

  @ViewChild('baseChart') chart: BaseChartDirective;

  constructor(
    private cryptoService: CryptoService
  ) {
  }

  ngOnInit() {
    this.chartData = [{
      data: [],
      label: 'BTC - USD'
    }, {
      data: [],
      label: 'ETC - USD'
    }];

    this.chartLabels = [0];
    this.prevInterval = 0;

    this.getCryptoValuesSubscription = this.cryptoService.cryptoValuesBehaviourSubject.subscribe(
      (response: CryptoModel) => {
        if (response !== null) {
          this.chartData[0].data.push(response.BTC.USD);
          this.chartData[1].data.push(response.ETH.USD);
          this.prevInterval = this.prevInterval + this.cryptoService.intervalPeriod / 1000;
          this.chartLabels.push(this.prevInterval);
          this.redrawChart();
        }
      }
    );
  }

  redrawChart() {
    if (this.chart !== undefined) {
      this.chart.ngOnDestroy();
      this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
    }
  }

}
