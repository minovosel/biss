import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CryptoService} from '../services/crypto.service';
import {CryptoModel} from '../models/crypto.model';
import {CryptoListModel} from '../models/crypto-list.model';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Subscription} from 'rxjs';
import {ExcelService} from '../services/excel.service';

@Component({
  selector: 'app-crypto-list',
  templateUrl: './crypto-list.component.html',
  styleUrls: ['./crypto-list.component.scss']
})
export class CryptoListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['cryptoName', 'usd', 'eur', 'hrk'];
  listData: CryptoListModel[] = [];
  dataSource = new MatTableDataSource([]);

  loadBy = 10;
  currentlyVisible = this.loadBy;
  filter: string;

  getCryptoValuesSubscription: Subscription;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private cryptoService: CryptoService,
    private excelService: ExcelService
  ) {
  }

  ngOnInit() {
    this.filter = '';

    this.getCryptoValuesSubscription = this.cryptoService.cryptoValuesBehaviourSubject.subscribe(
      (response: CryptoModel) => {
        if (response !== null) {
          for (const row of Object.keys(response)) {
            const item = {
              cryptoName: row,
              USD: response[row].USD,
              EUR: response[row].EUR,
              HRK: response[row].HRK
            };
            this.listData.unshift(item);
            this.filterResults(this.filter);
          }

          this.dataSource.data = this.listData.slice(0, this.currentlyVisible);
        }
      }
    );
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (data, attribute) => data[attribute];
    }, 1000);
  }

  ngOnDestroy(): void {
    this.getCryptoValuesSubscription.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.filter = filterValue;
    this.filterResults(this.filter);
  }

  filterResults(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onScroll() {
    if (this.currentlyVisible <= this.listData.length) {
      this.currentlyVisible += this.loadBy;
    }
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataSource.data, 'cryptoPrice');
  }

}
