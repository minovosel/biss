import {Component, OnInit} from '@angular/core';
import {CryptoService} from './services/crypto.service';
import {CryptoModel} from './models/crypto.model';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.scss']
})
export class CryptoComponent implements OnInit {

  constructor(
    private cryptoService: CryptoService
  ) {
  }

  ngOnInit() {
    this.cryptoService.getCryptoValue()
      .subscribe(
        (response: CryptoModel) => {
          this.cryptoService.cryptoValuesBehaviourSubject.next(response);
        }
      );
    this.cryptoService.getCryptoValuesInterval();
  }

}
