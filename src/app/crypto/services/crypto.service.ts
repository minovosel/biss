import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CryptoModel} from '../models/crypto.model';
import {BehaviorSubject, interval} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CryptoService implements OnDestroy {
  timer = 3600;
  intervalPeriod = 5000;
  getValuesInterval: any;

  cryptoValuesBehaviourSubject: BehaviorSubject<CryptoModel>;

  constructor(
    protected httpClient: HttpClient
  ) {
    this.cryptoValuesBehaviourSubject = new BehaviorSubject(null);
  }

  ngOnDestroy(): void {
    clearInterval(this.getValuesInterval);
  }

  getCryptoValuesInterval() {
    this.getValuesInterval = interval(this.intervalPeriod)
      .subscribe(
        () => {
          this.getCryptoValue().subscribe(
            (response: CryptoModel) => {
              this.cryptoValuesBehaviourSubject.next(response);
            }
          );
          this.timer -= this.intervalPeriod / 1000;
          if (this.timer <= 0) {
            clearInterval(this.getValuesInterval);
          }
        }
      );
  }

  getCryptoValue() {
    return this.httpClient.get<CryptoModel>(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH&tsyms=USD,EUR,HRK`);
  }
}
