import {NgModule} from '@angular/core';
import {CryptoListComponent} from './crypto-list/crypto-list.component';
import {CryptoComponent} from './crypto.component';
import {CryptoGraphComponent} from './crypto-graph/crypto-graph.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatSortModule, MatTableModule, MatTabsModule} from '@angular/material';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ChartsModule} from 'ng2-charts';
import {ExcelService} from './services/excel.service';

@NgModule({
  declarations: [
    CryptoListComponent,
    CryptoGraphComponent,
    CryptoComponent
  ],
  imports: [
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSortModule,
    InfiniteScrollModule,
    MatTabsModule,
    ChartsModule,
  ],
  providers: [
    ExcelService
  ],
  exports: [
    CryptoComponent
  ]
})

export class CryptoModule {
}
