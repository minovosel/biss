export class CryptoListModel {
  cryptoName: string;
  USD: number;
  EUR: number;
  HRK: number;

  constructor(cryptoListValues) {
    this.cryptoName = cryptoListValues.cryptoName || '';
    this.USD = cryptoListValues.USD || 0;
    this.EUR = cryptoListValues.EUR || 0;
    this.HRK = cryptoListValues.HRK || 0;
  }
}
