export class CryptoModel {
  BTC: CryptoValues;
  ETH: CryptoValues;

  constructor(cryptoValues) {
    this.BTC = cryptoValues.BTC || {};
    this.ETH = cryptoValues.ETH || {};
  }
}

interface CryptoValues {
  USD: number;
  EUR: number;
  HRK: number;
}
